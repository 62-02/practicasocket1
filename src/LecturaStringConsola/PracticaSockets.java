/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LecturaStringConsola;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
/**
 *
 * @author Jonha
 */
public class PracticaSockets {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        String cadena;
        BufferedReader br;
        
        br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Empieza escribir 'stop' para salir");
        do{
            cadena = br.readLine();
            System.out.println(cadena);
        }while(!cadena.equals("stop"));
        
    }
}

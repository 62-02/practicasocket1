package practicapatrickmk1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
public class PracticaServer {
    public static void main(String[] args) throws IOException {
        String cadena;
        BufferedReader br;
        
        br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Empieza a escribir, 'Stop' para salir");
        do {
            cadena = br.readLine();
            System.out.println(cadena);
        } while(!cadena.equals("Stop"));
    }
    
}
